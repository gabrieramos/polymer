//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//app.use(express.static(__dirname + '/build/v3')); //toma en cuenta la aplicación empaquetada en un release
app.use(express.static(__dirname + '/build/default')); 

app.listen(port);

console.log('Ejecutando polymer desde node: ' + port);

app.get("/", function(req, res){
  res.sendFile("index.html", {root: '.'}); //con root decimos que la raíz se encuentra donde actualmente estamos parados
});
